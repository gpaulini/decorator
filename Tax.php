<?php

/**
 * Interface de imposto.
 * Define os requisitos de um imposto.
 *
 * @author Pablo R. Quadrado <dev.pabloquadrado@gmail.com>
 */
interface Tax {

    /**
     * Aplica imposto sobre um orçamento.
     *
     * @param Budget $budget
     *
     * @return float
     */
	public function applyTax(Budget $budget);
}