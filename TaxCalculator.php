<?php

/**
 * Calculadora de impostos.
 *
 * @author Pablo R. Quadrado <dev.pabloquadrado@gmail.com>
 */
class TaxCalculator {

    /**
     * Retorna o valor dos impostos sobre um orçamento.
     *
     * @param Budget $budget Orçamento.
     * @param Tax $tax Impostos que serão calculados.
     *
     * @return float
     */
	public function getTaxesAmount(Budget $budget, Tax $tax)
	{
		return $tax->applyTax($budget);
	}
}