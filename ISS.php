<?php

/**
 * Imposto de ISS.
 *
 * @author Pablo R. Quadrado <dev.pabloquadrado@gmail.com>
 */
class ISS extends TaxDecorator {

    /**
     * @inheritDoc
     */
    protected function useMaximumTax(Budget $budget)
    {
        return $budget->getPrice() > 500;
    }

    /**
     * @inheritDoc
     */
    protected function maximumTax(Budget $budget)
    {
        return $budget->getPrice() * 0.1;
    }

    /**
     * @inheritDoc
     */
    protected function minimumTax(Budget $budget)
    {
        return $budget->getPrice() * 0.05;
    }
}