<?php

function loadFiles($class)
{
    require $class . '.php';
}

spl_autoload_register('loadFiles');