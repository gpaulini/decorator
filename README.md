# Atividade:

Uma grupo comercial brasieliro que está utilizando há meses este sistema, fechou uma parceira com uma empresa multinacional.

Para que o sistema atenda suas necessidades, esse grupo necessita que o imposto sobre operações financeiras (IOF) seja implementado com os seguintes requisitos:

    1. Valor máximo do imposto: 45%
    2. Valor mínimo do imposto: 18%



Orçamentos **acima de R$1.000,00** deverão receber **valor máximo do imposto**.