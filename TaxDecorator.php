<?php

/**
 * Decorador de impostos.
 *
 * @author Pablo R. Quadrado <dev.pabloquadrado@gmail.com>
 */
abstract class TaxDecorator implements Tax
{
    /** @var Tax Próxima desconto a ser aplicado. */
    protected $nextTax;

    /**
     * Construtor do decorador de impostos.
     *
     * Define a propriedade {@see AbstractTemplate::$tax}
     *
     * @param Tax|null $tax
     */
    public function __construct($tax = null)
    {
        $this->nextTax = $tax;
    }

    /**
     * @inheritDoc
     */
    public final function applyTax(Budget $budget)
    {
        if ($this->useMaximumTax($budget)) {
            return $this->maximumTax($budget) + $this->applyNextTax($budget);
        }

        return $this->minimumTax($budget) + $this->applyNextTax($budget);
    }

    /**
     * Aplica o próxima desconto, se houver.
     *
     * @param Budget $budget
     *
     * @return float|int
     */
    public function applyNextTax(Budget $budget)
    {
        if (! $this->nextTax) {
            return 0;
        }

        return $this->nextTax->applyTax($budget);
    }

    /**
     * Define se será aplicado o imposto máximo.
     *
     * @param Budget $budget
     *
     * @return bool
     */
    protected abstract function useMaximumTax(Budget $budget);

    /**
     * Retorna o valor máximo do imposto.
     *
     * @param Budget $budget
     *
     * @return float
     */
    protected abstract function maximumTax(Budget $budget);

    /**
     * Retorna o valor mínimo do imposto.
     *
     * @param Budget $budget
     *
     * @return float
     */
    protected abstract function minimumTax(Budget $budget);
}